---
layout: article
title:  "টেন্সরফ্লো দিয়ে সহজ বাংলায় ‘বাংলা’ ন্যাচারাল ল্যাঙ্গুয়েজ প্রসেসিং (এনএলপি)"
categories: articles
modified: 2016-06-01T16:28:57-04:00
share: false
ads: true
---

টেন্সরফ্লো দিয়ে সহজ বাংলায় ‘বাংলা’ ন্যাচারাল ল্যাঙ্গুয়েজ প্রসেসিং (এনএলপি)

বইটা লিখতে চেয়েছিলাম সামনের বছর। তবে, এই ৫০ বছর বয়সে সময় পাওয়া দুস্কর। তবে, লকডাউনের সুবিধা নিয়ে এই বইটায় হাত দেয়া।

[ আমি বই লিখি না, বরং ৫০ বছরের এক্সপেরিয়েন্স শেয়ার করি] কিছুটা কানেক্টিং দ্য ডটের মতো।

এই নোটবুকগুলো একটা বইয়ে দাড়িয়ে যাবে। ভিডিও/অডিও পডকাস্ট পাবেন;

{% include toc.html %}

## Sample Heading

### Sample Heading 2

Jekyll also offers powerful support for code snippets:

```ruby
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
```

Check out the [Jekyll docs][jekyll] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll's GitHub repo][jekyll-gh].

[jekyll-gh]: https://github.com/jekyll/jekyll
[jekyll]:    http://jekyllrb.com
